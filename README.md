# a list of required jars for groimp

These libraries can be added to a maven build by adding the repository :

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/55048746/-/packages/maven</url>
  </repository>
  </repositories>
```

then they can be added as maven dependency (e.g. for raskob):

```xml
<dependency>
<groupId>de.grogra.third-party</groupId>
  <artifactId>raskob</artifactId>
  <version>1.0</version>
</dependency>
```

The third party library stored in this repository are availabled under the groupId: de.grogra.third-party, and artifactId: NAME_OF_THE_LIB. 

## Manually add a maven package 

A jar can be added as a maven package in the gitlab registry with the command (for gluegen-rt):

mvn deploy:deploy-file -DgroupId=de.grogra.third-party -DartifactId=gluegen-rt -Dversion=1.0 -Dpackaging=jar -Dfile=gluegen_rt-1.0.jar -Durl=https://gitlab.com/api/v4/projects/48702354/packages/maven -DrepositoryId=gitlab-maven

## Setup the gitlab token

To load without requiring an authentification from git each maven operation, you can setup a Private-Token in two steps:

1. Create a settings.xml file under your .m2 directory (the maven directory, usually in ~/.m2). Set a server id :

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
     <server>
       <id>gitlab-maven</id>
       <configuration>
         <httpHeaders>
           <property>
             <name>Private-Token</name>
             <value>XXXXXXXXXXXXXX</value>
           </property>
         </httpHeaders>
       </configuration>
     </server>
   </servers>
 </settings>
```
2. Get your private token. In gitlab under profile>preference>personal token, create a new personal token. The name does not matter. Add the right to read registry as a minimum right. Be sure to set the token after creation as you cannot retrieve it latter. Change the XXXXXXXXXXXXXX in the settings.xml file with your personal token.

Notice that the server id is _gitlab-maven_. This is the repository id set in the pom.xml files of the groimp plugins. If you change the server id here. You need to change it in the pom.xml files of groimp so the id match.
